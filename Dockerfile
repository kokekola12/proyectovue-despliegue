# Descarga la imagen de Node.js en la versión 20.7.0 y la usa como etapa de construcción (builder)
FROM node:20.7.0 AS builder

# Establece el directorio de trabajo dentro del contenedor en /app
WORKDIR /app

# Copia los archivos package.json y package-lock.json al directorio de trabajo del contenedor
COPY package*.json ./

# Ejecuta el comando para instalar las dependencias de Node.js
RUN npm i

# Copia todos los archivos del directorio actual al directorio de trabajo del contenedor
COPY . .

# Ejecuta el comando para construir la aplicación
RUN npm run build

# Descarga la imagen más reciente de Nginx
FROM nginx:latest

# Copia el archivo de configuración de Nginx al directorio de configuración del contenedor
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf

# Copia los archivos construidos en la etapa de construcción al directorio donde Nginx sirve archivos estáticos
COPY --from=builder /app/dist /usr/share/nginx/html

# Indica los puertos TCP/IP a los cuales se puede acceder a los servicios del contenedor
EXPOSE 80

# Establece el comando del proceso de inicio del contenedor
CMD ["nginx", "-g", "daemon off;"]
